import { Component, OnInit } from '@angular/core';
import { UserService } from '../services/user.service';

@Component({
  selector: 'app-get-user',
  templateUrl: './get-user.component.html',
  styleUrls: ['./get-user.component.css']
})
export class GetUserComponent implements OnInit {
  fetchedUser: any;
  message: string;

  constructor(private userService: UserService) { }

  ngOnInit() {
  }

  getUserById(userId: number): boolean {
    this.userService.getById(userId).subscribe(
      res => {
        if (res.status === 200) {
          this.fetchedUser = res.body;
          this.message = 'User fetched successfully.'
        }
      },
      err => {
        this.message = err;
      },
      () => {}
    )
    return true;
  }

}
