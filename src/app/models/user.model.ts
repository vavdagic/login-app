export class User {
    firstname: string;
    lastname: string;
    username: string;
    password: string;

    constructor(userObject: any) {
        this.firstname = userObject.firstname;
        this.lastname = userObject.lastname;
        this.username = userObject.username;
        this.password = userObject.password;
    }
}
