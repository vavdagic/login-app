import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { User } from '../models/user.model';
import { UserService } from "../services/user.service";

@Component({
  selector: 'app-edit-user',
  templateUrl: './edit-user.component.html',
  styleUrls: ['./edit-user.component.css']
})
export class EditUserComponent implements OnInit {
  editUserForm: FormGroup;
  message: string;

  constructor(private userService: UserService, formBuilder: FormBuilder) {
    this.editUserForm = formBuilder.group({
      'userid': ['', Validators.required],
      'editFirstname': ['', Validators.required],
      'editLastname': ['', Validators.required],
      'editUsername': ['', Validators.required],
      'editPassword': ['', Validators.required]
    });
  }

  ngOnInit() {
  }

  editUserById(userData: any) {
    this.userService.update(
      userData.userid,
      new User({
        firstname: userData.editFirstname,
        lastname: userData.editLastname,
        username: userData.editUsername,
        password: userData.editPassword,
      })
    ).subscribe(
      res => {
        if (res.status === 200) {
          this.message = `User with ID ${userData.userid} edited successfully.`
        }
      },
      err => {
        this.message = err;
      },
      () => { }
    )

  }

}
