import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { User } from '../models/user.model';
import { UserService } from "../services/user.service";

@Component({
  selector: 'app-create-user',
  templateUrl: './create-user.component.html',
  styleUrls: ['./create-user.component.css']
})
export class CreateUserComponent implements OnInit {
  createUserForm: FormGroup;
  message: string;

  constructor(formBuilder: FormBuilder, private userService: UserService) {
    this.createUserForm = formBuilder.group({
      'firstname': ['', Validators.required],
      'lastname': ['', Validators.required],
      'username': ['', Validators.required],
      'password': ['', Validators.required]
    });
   }

  ngOnInit() {
    this.createUser({
      firstname: 'Test',
      lastname: 'Test',
      username: 'user@mail.com',
      password: 'pass123',
    })
  }

  createUser(userData: any) {
    this.userService.create(
      new User({
        firstname: userData.firstname,
        lastname: userData.lastname,
        username: userData.username,
        password: userData.password,
      })
    ).subscribe(
      res => {
        if (res.status == 200) {
          this.message = 'User created successfully.'
        }
      },
      err => {
        this.message = err;
      });
  }

}
