import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthService } from '../services/auth.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  message: string;

  constructor(formBuilder: FormBuilder, private authService: AuthService, private router: Router) {
    this.loginForm = formBuilder.group({
      'username': ['', Validators.required],
      'password': ['', Validators.required]
    });
  }

  ngOnInit() {
    this.logout();
  }

  logout(): boolean {
    this.authService.logout();
    return false;
  }

  login(formValues: any): boolean {
    this.authService.login(formValues.username, formValues.password).subscribe(
      res => {
        if (res) {
          this.router.navigate(['/home']);
        }
      },
      err => this.message = err
    );
    return false;
  }

}
