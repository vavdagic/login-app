import { Component, OnInit } from '@angular/core';
import { UserService } from "../services/user.service";

@Component({
  selector: 'app-list-users',
  templateUrl: './list-users.component.html',
  styleUrls: ['./list-users.component.css']
})
export class ListUsersComponent implements OnInit {
  allUsers: any;
  message: string;

  constructor(private userService: UserService) { }

  ngOnInit() {
  }

  listUsers() {
    this.userService.getAll().subscribe(
      res => {
        if (res.status === 200) {
          this.allUsers = res.body;
          this.message = 'User list fetched successfully.'
        }
    });
  }

}
