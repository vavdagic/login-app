import { Component, OnInit } from '@angular/core';
import { UserService } from '../services/user.service';

@Component({
  selector: 'app-delete-user',
  templateUrl: './delete-user.component.html',
  styleUrls: ['./delete-user.component.css']
})
export class DeleteUserComponent implements OnInit {
  message: string;

  constructor(private userService: UserService) { }

  ngOnInit() {
  }

  deleteUser(userId: number): boolean {
    console.log(userId);
    this.userService.delete(userId).subscribe(
      
      res => {
        console.log(res);
        this.message = res.status === 200 ? 'User deleted successfully.' : 'User deletion failed.';
      }
    )
    return false;
  }

}
