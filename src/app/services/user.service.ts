import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { User } from '../models/user.model';

@Injectable()
export class UserService {

    constructor(private http: HttpClient) { }

    getAll() {
        return this.http.get<User[]>('/api/users', {observe: 'response'});
    }

    getById(id: number) {
        return this.http.get('/api/users/' + id, {observe: 'response'});
    }

    create(user: User) {
        return this.http.post('/api/users', user, {observe: 'response'});
    }

    update(id: number, user: User) {
        return this.http.put('/api/users/'+ id, user, {observe: 'response'});
    }

    delete(id: number) {
        return this.http.delete('/api/users/' + id, {observe: 'response'});
    }
}
