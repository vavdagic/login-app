﻿import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import 'rxjs/add/operator/map';

@Injectable()
export class AuthService {
    constructor(private http: HttpClient) { }

    login(username: string, password: string) {
        return this.http.post<any>('/api/authenticate', { username: username, password: password })
            .map(user => {
                if (user) {
                    localStorage.setItem('currentUser', JSON.stringify(user));
                    return true;
                }
            });
    }

    logout() {
        localStorage.removeItem('currentUser');
    }

    getCurrentUser(): any {
        return JSON.parse(localStorage.getItem('currentUser'));
      }
}
